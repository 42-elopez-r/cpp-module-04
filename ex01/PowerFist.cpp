/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   PowerFist.cpp                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/06/24 13:52:55 by elopez-r          #+#    #+#             */
/*   Updated: 2021/06/24 18:22:16 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "PowerFist.hpp"
#include <iostream>

using std::cout; using std::endl;

PowerFist::PowerFist(): AWeapon("Power Fist", 8, 50)
{

}

PowerFist::PowerFist(const PowerFist& powerFist):
	AWeapon(powerFist.name, powerFist.apcost, powerFist.damage)
{
	*this = powerFist;
}

PowerFist::~PowerFist()
{

}

PowerFist&
PowerFist::operator=(const PowerFist& powerFist)
{
	name = powerFist.name;
	apcost = powerFist.apcost;
	damage = powerFist.damage;
	return (*this);
}

void
PowerFist::attack() const
{
	cout << "* pschhh... SBAM! *" << endl;
}
