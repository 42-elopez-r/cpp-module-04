/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   SuperMutant.cpp                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/06/24 18:09:01 by elopez-r          #+#    #+#             */
/*   Updated: 2021/06/24 18:17:59 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "SuperMutant.hpp"
#include <iostream>

using std::cout; using std::endl;

SuperMutant::SuperMutant(): Enemy(170, "Super Mutant")
{
	cout << "Gaaah. Me want smash heads!" << endl;
}

SuperMutant::SuperMutant(const SuperMutant& superMutant):
	Enemy(superMutant.hp, superMutant.type)
{
	*this = superMutant;
	cout << "Gaaah. Me want smash heads!" << endl;
}

SuperMutant::~SuperMutant()
{
	cout << "Aaargh..." << endl;
}

SuperMutant&
SuperMutant::operator=(const SuperMutant& superMutant)
{
	hp = superMutant.hp;
	type = superMutant.hp;
	return (*this);
}

void
SuperMutant::takeDamage(int damage)
{
	Enemy::takeDamage(damage - 3);
}
