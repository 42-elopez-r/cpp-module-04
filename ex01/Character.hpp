/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Character.hpp                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/06/24 18:43:22 by elopez-r          #+#    #+#             */
/*   Updated: 2021/06/24 19:38:39 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef CHARACTER_HPP
#define CHARACTER_HPP

#include <string>
#include <iostream>
#include "AWeapon.hpp"
#include "Enemy.hpp"

using std::string; using std::ostream;

class Character
{
	private:
		static const int MAX_AP;

		string name;
		int ap;
		AWeapon* weapon;

		Character();
	public:
		Character(const string& name);
		Character(const Character& character);
		~Character();
		Character& operator=(const Character& character);
		void recoverAP();
		void equip(AWeapon* weapon);
		void attack(Enemy* enemy);
		const string& getName() const;
		int getAP() const;
		AWeapon* getWeapon() const;
};

ostream& operator<<(ostream& os, const Character& character);

#endif
