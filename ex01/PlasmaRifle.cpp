/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   PlasmaRifle.cpp                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/06/24 13:52:55 by elopez-r          #+#    #+#             */
/*   Updated: 2021/06/24 21:02:56 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "PlasmaRifle.hpp"
#include <iostream>

using std::cout; using std::endl;

PlasmaRifle::PlasmaRifle(): AWeapon("Plasma Rifle", 5, 21)
{

}

PlasmaRifle::PlasmaRifle(const PlasmaRifle& plasmaRifle):
	AWeapon(plasmaRifle.name, plasmaRifle.apcost, plasmaRifle.damage)
{
	*this = plasmaRifle;
}

PlasmaRifle::~PlasmaRifle()
{

}

PlasmaRifle&
PlasmaRifle::operator=(const PlasmaRifle& plasmaRifle)
{
	name = plasmaRifle.name;
	apcost = plasmaRifle.apcost;
	damage = plasmaRifle.damage;
	return (*this);
}

void
PlasmaRifle::attack() const
{
	cout << "* piouuu piouuu piouuu *" << endl;
}
