/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   AWeapon.hpp                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/06/24 13:10:19 by elopez-r          #+#    #+#             */
/*   Updated: 2021/06/24 20:58:13 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef AWEAPON_HPP
#define AWEAPON_HPP

#include <string>

using std::string;

class AWeapon
{
	private:
		AWeapon();
	protected:
		string name;
		int apcost;
		int damage;
	public:
		AWeapon(const string& name, int apcost, int damage);
		AWeapon(const AWeapon& weapon);
		virtual ~AWeapon();
		AWeapon& operator=(const AWeapon& weapon);
		const string& getName() const;
		int getAPCost() const;
		int getDamage() const;
		virtual void attack() const = 0;
};

#endif
