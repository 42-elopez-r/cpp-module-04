/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   RadScorpion.cpp                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/06/24 18:09:01 by elopez-r          #+#    #+#             */
/*   Updated: 2021/06/24 18:34:46 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "RadScorpion.hpp"
#include <iostream>

using std::cout; using std::endl;

RadScorpion::RadScorpion(): Enemy(80, "RadScorpion")
{
	cout << "* click click click *" << endl;
}

RadScorpion::RadScorpion(const RadScorpion& radScorpion):
	Enemy(radScorpion.hp, radScorpion.type)
{
	*this = radScorpion;
	cout << "* click click click *" << endl;
}

RadScorpion::~RadScorpion()
{
	cout << "* SPROTCH *" << endl;
}

RadScorpion&
RadScorpion::operator=(const RadScorpion& radScorpion)
{
	hp = radScorpion.hp;
	type = radScorpion.hp;
	return (*this);
}
