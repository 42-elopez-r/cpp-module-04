/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/06/24 20:12:12 by elopez-r          #+#    #+#             */
/*   Updated: 2021/06/24 20:57:45 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Character.hpp"
#include "Enemy.hpp"
#include "PlasmaRifle.hpp"
#include "PowerFist.hpp"
#include "RadScorpion.hpp"
#include "SuperMutant.hpp"
#include <iostream>

using std::cout; using std::endl;

static void
attackSomething(Character* c, Enemy* e)
{
	cout << *c;
	c->attack(e);
	cout << *c;
	cout << e->getType() << " HP: " << e->getHP() << endl;
}

int
main()
{
	cout << " ==> Character creation" << endl;
	Character* me = new Character("me");
	cout << *me;

	cout << endl << " ==> Enemies creation" << endl;
	Enemy* r = new RadScorpion();
	SuperMutant* s = new SuperMutant();

	cout << endl << " ==> Weapons creation" << endl;
	AWeapon* pr = new PlasmaRifle();
	AWeapon* pf = new PowerFist();

	cout << endl << " ==> Try to attack without a weapon" << endl;
	cout << "SuperMutant HP: " << s->getHP() << endl;
	me->attack(s);
	cout << "SuperMutant HP: " << s->getHP() << endl;
	cout << *me;

	cout << endl << " ==> Equip a weapon" << endl;
	me->equip(pf);
	cout << *me;

	cout << endl << " ==> * DOOM music intensifies *" << endl;
	cout << " ==> Fight the RadScorpion" << endl;
	attackSomething(me, r);
	me->equip(pr);
	attackSomething(me, r);
	me->attack(r);
	cout << *me;

	cout << endl << " ==> Fight the SuperMutant" << endl;
	me->equip(pf);
	attackSomething(me, s);
	attackSomething(me, s);
	cout << endl << " ==> At this point the attack will fail because it doesn't"
		" have enough AP" << endl;
	attackSomething(me, s);
	cout << endl << " ==> Recover AP" << endl;
	me->recoverAP();
	attackSomething(me, s);
	cout << *me;
	me->attack(s);
	cout << *me;

	cout << endl << " ==> Cleanup" << endl;
	delete (me);
	delete (pr);
	delete (pf);
	return 0;
}
