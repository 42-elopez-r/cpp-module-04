/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Enemy.cpp                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/06/24 17:52:21 by elopez-r          #+#    #+#             */
/*   Updated: 2021/06/24 18:22:42 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Enemy.hpp"

Enemy::Enemy(int hp, const string& type)
{
	this->hp = hp;
	this->type = type;
}

Enemy::Enemy(const Enemy& enemy)
{
	*this = enemy;
}

Enemy::~Enemy()
{

}

Enemy&
Enemy::operator=(const Enemy& enemy)
{
	hp = enemy.hp;
	type = enemy.type;
	return (*this);
}

const string&
Enemy::getType() const
{
	return (type);
}

int
Enemy::getHP() const
{
	return (hp);
}

void
Enemy::takeDamage(int damage)
{
	if (damage <= 0)
		return;

	if (hp - damage >= 0)
		hp -= damage;
	else
		hp = 0;
}
