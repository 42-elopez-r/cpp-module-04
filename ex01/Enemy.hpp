/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Enemy.hpp                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/06/24 17:43:20 by elopez-r          #+#    #+#             */
/*   Updated: 2021/06/24 18:45:43 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef ENEMY_HPP
#define ENEMY_HPP

#include <string>

using std::string;

class Enemy
{
	private:
		Enemy();
	protected:
		int hp;
		string type;
	public:
		Enemy(int hp, const string& type);
		Enemy(const Enemy& enemy);
		virtual ~Enemy();
		Enemy& operator=(const Enemy& enemy);
		const string& getType() const;
		int getHP() const;
		virtual void takeDamage(int damage);
};

#endif
