/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   AWeapon.cpp                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/06/24 13:22:07 by elopez-r          #+#    #+#             */
/*   Updated: 2021/06/24 18:21:42 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "AWeapon.hpp"

AWeapon::AWeapon(const string& name, int apcost, int damage)
{
	this->name = name;
	this->apcost = apcost;
	this->damage = damage;
}

AWeapon::AWeapon(const AWeapon& weapon)
{
	*this = weapon;
}

AWeapon::~AWeapon()
{

}

AWeapon&
AWeapon::operator=(const AWeapon& weapon)
{
	name = weapon.name;
	apcost = weapon.apcost;
	damage = weapon.damage;
	return (*this);
}

const string&
AWeapon::getName() const
{
	return (name);
}

int
AWeapon::getAPCost() const
{
	return (apcost);
}

int
AWeapon::getDamage() const
{
	return (damage);
}
