/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Character.cpp                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/06/24 19:36:12 by elopez-r          #+#    #+#             */
/*   Updated: 2021/06/24 20:10:58 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Character.hpp"

using std::cout; using std::endl;

const int Character::MAX_AP = 40;

Character::Character(const string& name)
{
	this->name = name;
	ap = MAX_AP;
	weapon = NULL;
}

Character::Character(const Character& character)
{
	*this = character;
}

Character::~Character()
{

}

Character&
Character::operator=(const Character& character)
{
	name = character.name;
	ap = character.ap;
	weapon = character.weapon;
	return (*this);
}

void
Character::recoverAP()
{
	if (ap + 10 <= MAX_AP)
		ap += 10;
	else
		ap = MAX_AP;
}

void
Character::equip(AWeapon* weapon)
{
	this->weapon = weapon;
}

void
Character::attack(Enemy* enemy)
{
	if (!weapon || !enemy || ap < weapon->getAPCost())
		return;

	cout << name << " attacks " << enemy->getType() << " with a " << weapon->getName() << endl;
	weapon->attack();
	enemy->takeDamage(weapon->getDamage());
	ap -= weapon->getAPCost();
	if (enemy->getHP() == 0)
		delete (enemy);
}

const string&
Character::getName() const
{
	return (name);
}

int
Character::getAP() const
{
	return (ap);
}

AWeapon*
Character::getWeapon() const
{
	return (weapon);
}

ostream&
operator<<(ostream& os, const Character& character)
{
	os << character.getName() << " has " << character.getAP();
	if (character.getWeapon())
		os << " AP and wields a " << character.getWeapon()->getName() << endl;
	else
		os << " AP and is unarmed" << endl;

	return (os);
}
