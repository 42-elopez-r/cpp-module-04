/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   AssaultTerminator.cpp                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/06/27 17:07:00 by elopez-r          #+#    #+#             */
/*   Updated: 2021/06/27 17:52:26 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "AssaultTerminator.hpp"
#include <iostream>

using std::cout; using std::endl;

AssaultTerminator::AssaultTerminator()
{
	cout << "* teleports from space *" << endl;
}

AssaultTerminator::AssaultTerminator(const AssaultTerminator& assaultTerminator)
{
	*this = assaultTerminator;
	cout << "* teleports from space *" << endl;
}

AssaultTerminator::~AssaultTerminator()
{
	cout << "I'll be back..." << endl;
}

AssaultTerminator&
AssaultTerminator::operator=(const AssaultTerminator& assaultTerminator)
{
	(void)assaultTerminator; // Nothing to copy
	return (*this);
}

AssaultTerminator*
AssaultTerminator::clone() const
{
	return (new AssaultTerminator(*this));
}

void
AssaultTerminator::battleCry() const
{
	cout << "This code is unclean. PURIFY IT!" << endl;
}

void
AssaultTerminator::rangedAttack() const
{
	cout << "* does nothing *" << endl;
}

void
AssaultTerminator::meleeAttack() const
{
	cout << "* attacks with chainfists *" << endl;
}
