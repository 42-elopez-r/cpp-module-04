/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/06/27 22:00:35 by elopez-r          #+#    #+#             */
/*   Updated: 2021/06/28 23:41:44 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "TacticalMarine.hpp"
#include "AssaultTerminator.hpp"
#include "Squad.hpp"
#include <iostream>

using std::cout; using std::endl;

void
iterate_squad(ISquad* sqd)
{
	ISpaceMarine* cur;

	for (int i = 0; i < sqd->getCount(); i++)
	{
		cur = sqd->getUnit(i);

		cout << " => Index: " << i << " Pointer: " << cur << endl;
		cur->battleCry();
		cur->rangedAttack();
		cur->meleeAttack();
	}
}

int
main()
{
	ISpaceMarine* bob = new TacticalMarine;
	ISpaceMarine* jim = new AssaultTerminator;
	ISquad* vlc = new Squad;
	ISquad* vlc_copy;

	cout << "Squad count after adding bob: " << vlc->push(bob) << endl;
	cout << "Squad count after adding jim: " << vlc->push(jim) << endl;
	cout << "Squad count after trying to add bob again: " << vlc->push(bob) << endl;

	cout << endl << "Iterating the squad:" << endl;
	iterate_squad(vlc);

	cout << endl << "Copying the squad:" << endl;
	vlc_copy = new Squad(*(static_cast<Squad*>(vlc)));
	cout << "Iterating the copy:" << endl;
	iterate_squad(vlc_copy);

	cout << endl << "Assigning the squad to the copy so it gets overwritten:" << endl;
	*(static_cast<Squad*>(vlc_copy)) = *(static_cast<Squad*>(vlc));
	cout << "Iterating the new copy:" << endl;
	iterate_squad(vlc_copy);

	cout << endl << "Deleting squad:" << endl;
	delete vlc;
	cout << endl << "Deleting squad's copy:" << endl;
	delete vlc_copy;
}
