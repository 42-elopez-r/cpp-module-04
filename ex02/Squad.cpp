/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Squad.cpp                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/06/27 19:36:07 by elopez-r          #+#    #+#             */
/*   Updated: 2021/06/28 23:37:09 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Squad.hpp"
#include <cstddef>

void
Squad::delete_array()
{
	for (int i = 0; i < count; i++)
		delete array[i];
	delete[] array;
}

Squad::Squad()
{
	array = NULL;
	count = 0;
}

Squad::Squad(const Squad& squad)
{
	array = NULL;
	count = 0;
	*this = squad;
}

Squad::~Squad()
{
	delete_array();
}

Squad&
Squad::operator=(const Squad& squad)
{
	delete_array();

	count = squad.count;
	array = new ISpaceMarine*[count];
	for (int i = 0; i < count; i++)
		array[i] = squad.array[i]->clone();

	return (*this);
}

int
Squad::getCount() const
{
	return (count);
}

ISpaceMarine*
Squad::getUnit(int index) const
{
	if (index < 0 || index >= count)
		return (NULL);
	return (array[index]);
}

int
Squad::push(ISpaceMarine* spaceMarine)
{
	ISpaceMarine** newArray;

	if (!spaceMarine)
		return (count);
	// Check if its already in the array
	for (int i = 0; i < count; i++)
		if (array[i] == spaceMarine)
			return (count);

	newArray = new ISpaceMarine*[count + 1];
	for (int i = 0; i < count; i++)
		newArray[i] = array[i];
	newArray[count] = spaceMarine;

	delete[] array;
	array = newArray;
	count++;

	return (count);
}
