/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   TacticalMarine.cpp                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/06/27 17:07:00 by elopez-r          #+#    #+#             */
/*   Updated: 2021/06/27 17:47:48 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "TacticalMarine.hpp"
#include <iostream>

using std::cout; using std::endl;

TacticalMarine::TacticalMarine()
{
	cout << "Tactical Marine ready for battle!" << endl;
}

TacticalMarine::TacticalMarine(const TacticalMarine& tacticalMarine)
{
	*this = tacticalMarine;
	cout << "Tactical Marine ready for battle!" << endl;
}

TacticalMarine::~TacticalMarine()
{
	cout << "Aaargh..." << endl;
}

TacticalMarine&
TacticalMarine::operator=(const TacticalMarine& tacticalMarine)
{
	(void)tacticalMarine; // Nothing to copy
	return (*this);
}

TacticalMarine*
TacticalMarine::clone() const
{
	return (new TacticalMarine(*this));
}

void
TacticalMarine::battleCry() const
{
	cout << "For the holy PLOT!" << endl;
}

void
TacticalMarine::rangedAttack() const
{
	cout << "* attacks with a bolter *" << endl;
}

void
TacticalMarine::meleeAttack() const
{
	cout << "* attacks with a chainsword *" << endl;
}
