/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Squad.hpp                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/06/27 19:26:18 by elopez-r          #+#    #+#             */
/*   Updated: 2021/06/27 21:47:58 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef SQUAD_HPP
#define SQUAD_HPP

#include "ISquad.hpp"
#include "ISpaceMarine.hpp"

class Squad: public ISquad
{
	private:
		ISpaceMarine** array;
		int count;

		void delete_array();
	public:
		Squad();
		Squad(const Squad& squad);
		~Squad();
		Squad& operator=(const Squad& squad);
		int getCount() const;
		ISpaceMarine* getUnit(int index) const;
		int push(ISpaceMarine* spaceMarine);
};

#endif
