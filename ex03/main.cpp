/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/07/03 13:04:37 by elopez-r          #+#    #+#             */
/*   Updated: 2021/07/03 16:02:39 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "MateriaSource.hpp"
#include "Ice.hpp"
#include "Cure.hpp"
#include "Character.hpp"
#include <iostream>

using std::cout; using std::endl;

void listEquip(ICharacter* ch)
{
	AMateria* tmp;

	cout << "Character \"" << ch->getName() << "\" equipped with:" << endl;

	for (int i = 0; (tmp = ch->getEquip(i)); i++)
	{
		cout << "Slot " << i << ": Type " << tmp->getType();
		cout << " " << tmp->getXP() << " XP" << endl;
	}
}

int
main()
{
	// Create MateriaSource
	IMateriaSource* src = new MateriaSource();
	src->learnMateria(new Ice());
	src->learnMateria(new Cure());

	// Create Character
	ICharacter* me = new Character("me");

	// Equip the character
	AMateria* tmp;
	tmp = src->createMateria("ice");
	me->equip(tmp);
	tmp = src->createMateria("cure");
	for (int i = 0; i < 3; i++)  // Try to equip the same mat. object multiple times
		me->equip(tmp);
	for (int i = 0; i < 10; i++)  // Try to equip more than four materias
	{
		tmp = src->createMateria("ice");
		me->equip(tmp);
	}
	listEquip(me);

	cout << endl << "Unequip some of them" << endl;
	for (int i = 6; i >= 2; i--)
		me->unequip(i);
	listEquip(me);

	// Create target character and use the equipped materia on him
	ICharacter* bob = new Character("bob");
	cout << endl << "Use equipped materia on target bob" << endl;
	me->use(0, *bob);
	me->use(1, *bob);

	cout << endl << "Equipment after use:" << endl;
	listEquip(me);

	delete bob;
	delete me;
	delete src;

	return 0;
}
