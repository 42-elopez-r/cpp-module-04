/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   AMateria.hpp                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/06/29 13:59:27 by elopez-r          #+#    #+#             */
/*   Updated: 2021/07/02 13:04:33 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef AMATERIA_HPP
#define AMATERIA_HPP

#include <string>

using std::string;

class ICharacter;

class AMateria
{
	private:
		AMateria();

	protected:
		string _type;
		unsigned int _xp;

	public:
		AMateria(const string& type);
		AMateria(const AMateria& materia);
		virtual ~AMateria();
		AMateria& operator=(const AMateria& materia);
		const string& getType() const;
		unsigned int getXP() const;
		virtual AMateria* clone() const = 0;
		virtual void use(ICharacter& target);
};

#endif
