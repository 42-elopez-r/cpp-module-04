/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   MateriaSource.hpp                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/07/02 21:24:12 by elopez-r          #+#    #+#             */
/*   Updated: 2021/07/02 21:41:00 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef MATERIA_SOURCE_HPP
#define MATERIA_SOURCE_HPP

#include "IMateriaSource.hpp"

class MateriaSource: public IMateriaSource
{
	private:
		AMateria* _learnt[4];
		int _ammount;
	public:
		MateriaSource();
		MateriaSource(const MateriaSource& materiaSource);
		~MateriaSource();
		MateriaSource& operator=(const MateriaSource& materiaSource);
		void learnMateria(AMateria* materia);
		AMateria* createMateria(const string& type);
};

#endif
