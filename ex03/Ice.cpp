/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Ice.cpp                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/07/02 12:44:52 by elopez-r          #+#    #+#             */
/*   Updated: 2021/07/03 13:12:28 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Ice.hpp"
#include "ICharacter.hpp"
#include <iostream>

using std::cout; using std::endl;

Ice::Ice(): AMateria("ice")
{

}

Ice::Ice(const Ice& ice): AMateria("ice")
{
	AMateria::operator=(ice);
}

Ice::~Ice()
{

}

Ice&
Ice::operator=(const Ice& ice)
{
	AMateria::operator=(ice);

	return (*this);
}

Ice*
Ice::clone() const
{
	return new Ice(*this);
}

void
Ice::use(ICharacter& target)
{
	AMateria::use(target);
	cout << "* shoots an ice bolt at " << target.getName() << " *" << endl;
}
