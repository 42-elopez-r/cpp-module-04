/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Cure.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/07/02 12:44:52 by elopez-r          #+#    #+#             */
/*   Updated: 2021/07/02 13:12:19 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Cure.hpp"
#include "ICharacter.hpp"
#include <iostream>

using std::cout; using std::endl;

Cure::Cure(): AMateria("cure")
{

}

Cure::Cure(const Cure& cure): AMateria("cure")
{
	AMateria::operator=(cure);
}

Cure::~Cure()
{

}

Cure&
Cure::operator=(const Cure& cure)
{
	AMateria::operator=(cure);

	return (*this);
}

Cure*
Cure::clone() const
{
	return new Cure(*this);
}

void
Cure::use(ICharacter& target)
{
	AMateria::use(target);
	cout << "* heals " << target.getName() << " wounds *" << endl;
}
