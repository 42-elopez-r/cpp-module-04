/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   MateriaSource.cpp                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/07/02 21:28:11 by elopez-r          #+#    #+#             */
/*   Updated: 2021/07/02 21:42:11 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "MateriaSource.hpp"
#include <cstddef>

MateriaSource::MateriaSource()
{
	_ammount = 0;
}

MateriaSource::MateriaSource(const MateriaSource& materiaSource)
{
	_ammount = 0;
	*this = materiaSource;
}

MateriaSource::~MateriaSource()
{
	for (int i = 0; i < _ammount; i++)
		delete _learnt[i];
}

MateriaSource&
MateriaSource::operator=(const MateriaSource& materiaSource)
{
	for (int i = 0; i < _ammount; i++)
		delete _learnt[i];

	_ammount = materiaSource._ammount;
	for (int i = 0; i < _ammount; i++)
		_learnt[i] = materiaSource._learnt[i]->clone();

	return (*this);
}

void
MateriaSource::learnMateria(AMateria* materia)
{
	if (_ammount >= 4)
		return;
	for (int i = 0; i < _ammount; i++)
		if (_learnt[i] == materia)
			return;

	_learnt[_ammount++] = materia;
}

AMateria*
MateriaSource::createMateria(const string& type)
{
	for (int i = 0; i < _ammount; i++)
		if (_learnt[i]->getType() == type)
			return (_learnt[i]->clone());
	return (NULL);
}
