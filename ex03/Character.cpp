/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Character.cpp                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/07/02 20:32:50 by elopez-r          #+#    #+#             */
/*   Updated: 2021/07/03 14:47:49 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Character.hpp"

Character::Character(const string& name)
{
	_name = name;
	_ammount = 0;
}

Character::Character(const Character& character)
{
	_ammount = 0;
	*this = character;
}

Character::~Character()
{
	for (int i = 0; i < _ammount; i++)
		delete _inventory[i];
}

Character&
Character::operator=(const Character& character)
{
	for (int i = 0; i < _ammount; i++)
		delete _inventory[i];

	_name = character._name;
	_ammount = character._ammount;
	for (int i = 0; i < _ammount; i++)
		_inventory[i] = character._inventory[i]->clone();

	return (*this);
}

const string&
Character::getName() const
{
	return (_name);
}

void
Character::equip(AMateria* m)
{
	if (_ammount >= 4)
		return;
	for (int i = 0; i < _ammount; i++)
		if (_inventory[i] == m)
			return;

	_inventory[_ammount++] = m;
}

void
Character::unequip(int idx)
{
	if (idx >= 0 && idx < _ammount)
	{
		for (int i = idx + 1; i < _ammount; i++)
			_inventory[i - 1] = _inventory[i];
		_ammount--;
	}
}

void
Character::use(int idx, ICharacter& target)
{
	if (idx >= 0 && idx < _ammount)
		_inventory[idx]->use(target);
}

AMateria*
Character::getEquip(int idx)
{
	return ((idx >= 0 && idx < _ammount) ? _inventory[idx] : NULL);
}
