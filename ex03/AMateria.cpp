/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   AMateria.cpp                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/06/30 15:53:12 by elopez-r          #+#    #+#             */
/*   Updated: 2021/07/02 13:07:32 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "AMateria.hpp"
#include "ICharacter.hpp"

AMateria::AMateria(const string& type)
{
	_type = type;
	_xp = 0;
}

AMateria::AMateria(const AMateria& materia)
{
	_type = materia._type;
	_xp = materia._xp;
}

AMateria::~AMateria()
{

}

AMateria&
AMateria::operator=(const AMateria& materia)
{
	_xp = materia._xp;

	return (*this);
}

const string&
AMateria::getType() const
{
	return (_type);
}

unsigned int
AMateria::getXP() const
{
	return (_xp);
}

void
AMateria::use(ICharacter& target)
{
	(void)target;
	_xp += 10;
}
