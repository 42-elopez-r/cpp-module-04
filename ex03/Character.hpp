/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Character.hpp                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/07/02 13:13:49 by elopez-r          #+#    #+#             */
/*   Updated: 2021/07/03 14:46:12 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef CHARACTER_HPP
#define CHARACTER_HPP

#include "ICharacter.hpp"
#include "AMateria.hpp"
#include <string>

using std::string;

class Character: public ICharacter
{
	private:
		AMateria* _inventory[4];
		int _ammount;
		string _name;

		Character();
	public:
		Character(const string& name);
		Character(const Character& character);
		~Character();
		Character& operator=(const Character& character);
		const string& getName() const;
		void equip(AMateria* m);
		void unequip(int idx);
		void use(int idx, ICharacter& target);
		AMateria* getEquip(int idx);
};

#endif
