/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Victim.hpp                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/06/23 18:09:14 by elopez-r          #+#    #+#             */
/*   Updated: 2021/06/24 14:46:19 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef VICTIM_HPP
#define VICTIM_HPP

#include <string.h>
#include <iostream>

using std::string; using std::ostream;

class Victim
{
	private:
		Victim();
	protected:
		string name;
	public:
		Victim(const string& name);
		Victim(const Victim& victim);
		virtual ~Victim();
		Victim& operator=(const Victim& victim);
		const string& getName() const;
		virtual void getPolymorphed() const;
};

ostream& operator<<(ostream& os, const Victim& victim);

#endif
