/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Victim.cpp                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/06/23 18:14:46 by elopez-r          #+#    #+#             */
/*   Updated: 2021/06/24 13:46:18 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Victim.hpp"

using std::cout; using std::endl;

Victim::Victim(const string& name)
{
	this->name = name;
	cout << "Some random victim called " << name << " just appeared!" << endl;
}

Victim::Victim(const Victim& victim)
{
	*this = victim;
	cout << "Some random victim called " << name << " just appeared!" << endl;
}

Victim::~Victim()
{
	cout << "Victim " << name << " just died for no apparent reason!" << endl;
}

Victim&
Victim::operator=(const Victim& victim)
{
	name = victim.name;
	return (*this);
}

const string&
Victim::getName() const
{
	return (name);
}

void
Victim::getPolymorphed() const
{
	cout << name << " has been turned into a cute little sheep!" << endl;
}

ostream&
operator<<(ostream& os, const Victim& victim)
{
	os << "I'm " << victim.getName() << " and I like otters!" << endl;
	return (os);
}
