#include "Sorcerer.hpp"
#include "Victim.hpp"
#include "Peon.hpp"

int main()
{
	Sorcerer robert("Robert", "the Magnificent");

	Victim jim("Jimmy");
	Peon joe("Joe");
	Victim *joe_copy = new Peon(joe);

	std::cout << robert << jim << joe << *joe_copy;

	robert.polymorph(jim);
	robert.polymorph(joe);
	robert.polymorph(*joe_copy);

	delete joe_copy;
	return 0;
}
