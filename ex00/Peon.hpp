/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Peon.hpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/06/23 18:40:09 by elopez-r          #+#    #+#             */
/*   Updated: 2021/06/24 13:41:24 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PEON_HPP
#define PEON_HPP

#include "Victim.hpp"

class Peon: public Victim
{
	private:
		Peon();
	public:
		Peon(const string& name);
		Peon(const Peon& peon);
		~Peon();
		Peon& operator=(const Peon& peon);
		virtual void getPolymorphed() const;
};

#endif
