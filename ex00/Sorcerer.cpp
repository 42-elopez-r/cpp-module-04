/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Sorcerer.cpp                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/06/11 20:41:37 by elopez-r          #+#    #+#             */
/*   Updated: 2021/06/24 13:47:54 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Sorcerer.hpp"

using std::cout; using std::endl;

Sorcerer::Sorcerer(const string& name, const string& title)
{
	this->name = name;
	this->title = title;
	cout << this->name << ", " << this->title << ", is born!" << endl;
}

Sorcerer::Sorcerer(const Sorcerer& sorcerer)
{
	*this = sorcerer;
	cout << name << ", " << title << ", is born!" << endl;
}

Sorcerer::~Sorcerer()
{
	cout << name << ", " << title << ", is dead.";
	cout << " Consequences will never be the same!" << endl;
}

Sorcerer&
Sorcerer::operator=(const Sorcerer& sorcerer)
{
	name = sorcerer.name;
	title = sorcerer.title;
	return (*this);
}

const string&
Sorcerer::getName() const
{
	return (name);
}

const string&
Sorcerer::getTitle() const
{
	return (title);
}

void
Sorcerer::polymorph(const Victim& victim) const
{
	victim.getPolymorphed();
}

ostream&
operator<<(ostream& os, const Sorcerer& sorcerer)
{
	os << "I am " << sorcerer.getName() << ", " << sorcerer.getTitle();
	os << ", and I like ponies!" << endl;
	return (os);
}
