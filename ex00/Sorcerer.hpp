/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Sorcerer.hpp                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/06/11 20:34:47 by elopez-r          #+#    #+#             */
/*   Updated: 2021/06/24 13:38:35 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef SORCERER_HPP
#define SORCERER_HPP

#include <string>
#include <iostream>
#include "Victim.hpp"

using std::string; using std::ostream;

class Sorcerer
{
	private:
		string name;
		string title;

		Sorcerer();
	public:
		Sorcerer(const string& name, const string& title);
		Sorcerer(const Sorcerer& sorcerer);
		~Sorcerer();
		Sorcerer& operator=(const Sorcerer& sorcerer);
		const string& getName() const;
		const string& getTitle() const;
		void polymorph(const Victim& victim) const;
};

ostream& operator<<(ostream& os, const Sorcerer& sorcerer);

#endif
