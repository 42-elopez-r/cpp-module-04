/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Peon.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/06/23 18:46:43 by elopez-r          #+#    #+#             */
/*   Updated: 2021/06/24 13:47:09 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Peon.hpp"

using std::cout; using std::endl;

Peon::Peon(const string& name): Victim(name)
{
	cout << "Zog zog" << endl;
}

Peon::Peon(const Peon& peon): Victim(peon.name)
{
	*this = peon;
	cout << "Zog zog" << endl;
}

Peon::~Peon()
{
	cout << "Bleuark..." << endl;
}

Peon&
Peon::operator=(const Peon& peon)
{
	name = peon.name;
	return (*this);
}

void
Peon::getPolymorphed() const
{
	cout << name << " has been turned into a pink pony!" << endl;
}
